// https://www.npmjs.com/package/request-promise
const requestPromise = require('request-promise-native');
const _ = require('lodash');
const config = require('./config');
// Then intention behind the line below is to enable debug level logging with 'test' serving as an 
// effective namespace and convention e.g. $ DEBUG=test npm test
const debug = require('debug')('test');
const receiptPostData = require('./receiptTestData.js');

// Operating on a deep clone due to:
// 1. the possibility of module caching https://nodejs.org/api/modules.html
// 2. the cited module (cached object) is not immutable so if it is somehow cached and shared between test cases
//    we could have non determinitic test cases if one test case modifies the exact same cached object
const receiptPostDataClone = _.cloneDeep(receiptPostData);

var options = {
  method: 'POST',
  uri: config.rip.purchase.api.uri,
  body: receiptPostDataClone,
  headers: {
    'Content-Type': 'application/json',
    'x-api-key': config.rip.purchase.api.key
  },
  json: true // Automatically stringifies the body to JSON 
};

test('basic positive test case', () => {
  return requestPromise(options).then(parsedBody => {
    debug(parsedBody);
    expect(parsedBody.result).toMatch('success');
    expect(parsedBody.message).toMatch('Data Received');
  });
});