// https://www.npmjs.com/package/request-promise
const remoteRestApiCallWith = require('request-promise-native')
const _ = require('lodash')
const config = require('./config')
// Then intention behind the line below is to enable debug level logging with 'test' serving as an 
// effective namespace and convention e.g. $ DEBUG=test npm test
const debug = require('debug')('test')
const receiptPostData = require('./receiptTestData.js')

var receiptPostDataClone
var restCallData

beforeEach(() => {
  // Operating on a deep clone due to:
  // 1. the possibility of module caching https://nodejs.org/api/modules.html
  // 2. the cited module (cached object) is not immutable so if it is somehow cached and shared between test cases
  //    we could have non determinitic test cases if one test case modifies the exact same cached object
  receiptPostDataClone = _.cloneDeep(receiptPostData)
  
  restCallData = {
    method: 'POST',
    uri: config.rip.purchase.api.uri,
    body: receiptPostDataClone,
    headers: {
      'Content-Type': 'application/json',
      'x-api-key': config.rip.purchase.api.key
    },
    json: true // Automatically stringifies the body to JSON 
  }
})

test('the data is Data invoice header national invoice must be Boolean', () => {
  
  receiptPostDataClone.data.invoice.header.nationalInvoice = "STRING-NOT-BOOLEAN"

  expect.assertions(5)
  
  return remoteRestApiCallWith(restCallData).then(parsedBody => {
    // debug('parsedBody',parsedBody)
    // expect(parsedBody.result).toMatch('failure')
    // expect(parsedBody.error).toBeDefined()
    // expect(Object.keys(parsedBody.error).length).toBe(1)
    // expect(parsedBody.error['data.invoice.header.nationalInvoice']).toBeDefined()
    // expect(parsedBody.error['data.invoice.header.nationalInvoice']).toContain('Data invoice header national invoice must be Boolean')
  },
  err => {
    debug('err',err.error.errors)
    expect(err.statusCode).toBe(400)
    expect(err.error.errors).toBeDefined()
    expect(Object.keys(err.error.errors).length).toBe(1)
    expect(err.error.errors['data.invoice.header.nationalInvoice']).toBeDefined()
    expect(err.error.errors['data.invoice.header.nationalInvoice']).toContain('Data invoice header national invoice must be Boolean')
  })
})

test('if NOT a receipt many fields become required', () => {

  // If the below field is not 0 we are not dealing with a receipt, rather
  // and invoice
  receiptPostDataClone.data.invoice.totals.toPayInCentsInclTax = 100

  expect.assertions(7)
  
   return remoteRestApiCallWith(restCallData).then(parsedBody => {
  //   debug('parsedBody',parsedBody)
  //   // expect(parsedBody.result).toMatch('failure')
  //   // expect(parsedBody.error).toBeDefined()
  //   // expect(Object.keys(parsedBody.error).length).toBe(1)
  //   // expect(parsedBody.error['data.invoice.header.dueDate']).toBeDefined()
  //   // expect(parsedBody.error['data.invoice.header.dueDate']).toContain('dueDate is required if toPayInCentsInclTax is greater than 0')
  },
  err => {
    debug('err',err.error.errors)
    expect(err.statusCode).toBe(400)
    expect(err.error).toBeDefined()
    expect(Object.keys(err.error.errors).length).toBe(4)
    expect(err.error.errors['data.invoice.header.dueDate']).toContain('dueDate is required if toPayInCentsInclTax is greater than 0')
    expect(err.error.errors['data.invoice.header.recipientName']).toContain('Recipient name is required if toPayInCentsInclTax is greater than 0')
    expect(err.error.errors['data.invoice.header.recipientAddress']).toContain('Recipient address is required if toPayInCentsInclTax is greater than 0')
    expect(err.error.errors['data.invoice.header.invoiceNumber']).toContain('invoiceNumber is required if toPayInCentsInclTax is greater than 0')
  })
})