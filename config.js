const debug = require('debug')('config')

const { RIP_PURCHASE_API_URI, RIP_PURCHASE_API_KEY, RIP_PURCHASE_API_MERCHANT_ID } = process.env

// NOTE: 
// 1. By default configuration points to the internal environment
//    with a particular API Key and Merchant Id being used. It is expected
//    that CI environments will override this config with environment
//    variables.
// 2. In time we won't commit secrets to version control (it's a bad thing), for
//    now doing it.
const config = {

    rip: {

        purchase: {

            api: {

                uri: RIP_PURCHASE_API_URI || 'https://4ujcekd2p9.execute-api.us-east-1.amazonaws.com/internal/purchase',
                key: RIP_PURCHASE_API_KEY || 'mmZAxLNmh63uQdpzLwq5s5Avht5N8gQG6jKnf2Gf',

                merchant: {

                    id: RIP_PURCHASE_API_MERCHANT_ID || 'rip:us-east-1:06c1cb25-6de2-4c95-9f6e-7eaa0e85502b'

                }

            }
        }

    }
}

debug("URI: " + config.rip.purchase.api.uri);
debug("KEY: " + config.rip.purchase.api.key);
debug("MERCH ID: " + config.rip.purchase.api.merchant.id);

module.exports = config