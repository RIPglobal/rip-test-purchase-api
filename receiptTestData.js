const config = require('./config');
// NOTE: 
// 1. All optional fields have been removed and required fields are listed in the order they appear 
//    in the 24/08/2017 API spec. This file represents the bare minimum receipting fields.
// 2. The API is liberal in what it accepts in the sense that if you put in a random field it won't be 
//    rejected but we're not doing this here.
// 3. The cached object instance that this module returns is intended to be DEEP CLONED 
//    in each test case.
module.exports = {
  "specVersion": "1",
  "authentication": {
    // RIP specific field below ...
    "merchantId": config.rip.purchase.api.merchant.id
  },
  "data": {
    "customerIdentification": {
      "customerId": "RIP257"
    },
    "invoice": {
      "header": {
        "nationalInvoice": true,
        "taxNumber": "127-270-747",
        "merchantName": "ACME Butchers Ltd",
        "timestamp": "2017-08-07" // NOT ISO 8601 date + time compliant
      },
      "totals": {
        "amountInCentsExTax": "1000",
        "totalTaxInCents": "123",
        // Take note of toPayInCentsInclTax directly below - because its 0 a number of fields become optional is this
        // is also what makes this both a tax invoice and "receipt".
        "toPayInCentsInclTax": "0",
        "currency": "NZD",
        "totalInCentsInclTax": "1123"
      },
      "lines": [{
        "quantity": "7",
        "units": "kg",
        "code": "RUMP-PREMIUM",
        "description": "Premium NZ Thick Cut Rump Steak",
        "pricePerUnit": "234",
        "totalInCents": "1234",
        "taxInCents": "34",
        "currency": "NZD",
        // RIP specific field below ...
        "ripGlCode": "ABC123",
        // RIP specific field below ...
        "ripJustification": "Some justy"
      }]
    }
  }
};