# README #

Getting POS /purchase API testing up and running.

### What is this repository for? ###

/purchase API regression testing (integration testing)

### How do I get set up? ###

* npm install
* npm test

### How do I get CI set up? ###

You'll need to set these environment variables in your job(s) as they are required in config.js:

* RIP_PURCHASE_API_URI 
* RIP_PURCHASE_API_KEY
* RIP_PURCHASE_API_MERCHANT_ID

From there on you'll need to run $ npm test

### How to I make the logs verbose? ###

We're using the [debug](https://www.npmjs.com/package/debug) package with these conventions:

* TEST debug statements in *.test.js files are identified by the name: test
* CONFIG debug statements in config.js are identified by the name: config

Examples in order of increasing verbosity:

$ npm test
$ DEBUG=test npm test
$ DEBUG=config,test npm test

